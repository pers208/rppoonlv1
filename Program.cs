using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Program
    {

        static void Main(string[] args)
        {
            Note deathNote = new Note("Hello from the other side");
            Note schoolNote = new Note("Kyle", "They killed Kenny");
            Note businessCard = new Note("Tom", "Pls hire me", 4);
            Console.WriteLine(deathNote.GetAuthor());
            Console.WriteLine(deathNote.GetText());
            Console.WriteLine(schoolNote.GetAuthor());
            Console.WriteLine(schoolNote.GetText());
            Console.WriteLine(businessCard.GetAuthor());
            Console.WriteLine(businessCard.GetText());

            TimeNote note = new TimeNote("matej", "poy", 0);
            Console.WriteLine(note.ToString());

            List<TimeNote> tasks = new List<TimeNote>();
            ToDo todaysTasks = new ToDo(tasks);
            string author;
            string text;
            int level;
            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine("Enter your name:");
                author = Console.ReadLine();
                Console.WriteLine("Enter your input:");
                text = Console.ReadLine();
                Console.WriteLine("How important is this(0-not really,10-very):");
                level = Convert.ToInt32(Console.ReadLine());
                todaysTasks.AddTask(author, text, level);
            }

            Console.WriteLine(todaysTasks.GetAllTasks());

            int maxPriorityIndex = 0;
            int maxValue = tasks[0].GetImportanceLevel();
            for (int i = 0; i <= tasks.Count; i++)
            {
                maxValue = tasks[i].GetImportanceLevel();
                maxPriorityIndex = i;
                for (int j = 0; j < tasks.Count; j++)
                {
                    if (tasks[j].GetImportanceLevel() > maxValue)
                    {
                        maxValue = tasks[j].GetImportanceLevel();
                        maxPriorityIndex = j;
                    }
                }
                todaysTasks.CompleteTask(maxPriorityIndex);
            }
            todaysTasks.CompleteTask(0);

            Console.WriteLine(todaysTasks.GetAllTasks());
        }
    }
    
}
